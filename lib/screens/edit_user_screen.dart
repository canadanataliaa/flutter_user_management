import 'dart:async';
import 'package:flutter/material.dart';

import '/utils/api.dart';
import '/utils/functions.dart';
import '/models/user.dart';

class EditUserScreen extends StatefulWidget {
	final User _user;

	EditUserScreen(this._user);

	@override
	EditUserScreenState createState() => EditUserScreenState();
}

class EditUserScreenState extends State<EditUserScreen> {
	Future<User>? _futureUser;

	final _formKey = GlobalKey<FormState>();
	final _tffNameController = TextEditingController();
	final _tffEmailController = TextEditingController();
	final _tffPhoneController = TextEditingController();
	final _tffWebsiteController = TextEditingController();

	void _updateUser(BuildContext context) {
		User user = User(
			name: _tffNameController.text.isEmpty == true ? widget._user.name : _tffNameController.text,
			email: _tffEmailController.text.isEmpty == true ? widget._user.email : _tffEmailController.text,
			phone: _tffPhoneController.text.isEmpty == true ? widget._user.phone : _tffPhoneController.text,
			website: _tffWebsiteController.text.isEmpty == true ? widget._user.website : _tffWebsiteController.text
		);

		setState(() {
			_futureUser = API().updateUser(user, widget._user.id).catchError((err) {
				showSnackBar(context, err.message);
			});
		});
	}

	@override
	Widget build(BuildContext context) {
		Widget txtName = TextFormField(
			decoration: InputDecoration(labelText: widget._user.name, labelStyle: TextStyle(color: Colors.black)),
			controller: _tffNameController,
		);

		Widget txtEmail = TextFormField(
			decoration: InputDecoration(labelText: widget._user.email, labelStyle: TextStyle(color: Colors.black)),
			controller: _tffEmailController,
            validator: (value) {
				return value != null && value.isNotEmpty && value.contains('@') ? null : 'Email is invalid.';
			}
		);

		Widget txtPhone = TextFormField(
			decoration: InputDecoration(labelText: widget._user.phone, labelStyle: TextStyle(color: Colors.black)),
			controller: _tffPhoneController,
		);

		Widget txtWebsite = TextFormField(
			decoration: InputDecoration(labelText: widget._user.website, labelStyle: TextStyle(color: Colors.black)),
			controller: _tffWebsiteController,
		);

		Widget btnUpdate = Container(
			width: double.infinity,
			margin: EdgeInsets.only(top: 10.0),
			child: ElevatedButton(
				child: Text('Update'),
				onPressed: () {
					if (_formKey.currentState!.validate()) {
						_updateUser(context);
					} else {
						showSnackBar(context, 'Invalid inputs.');
					}
				}
			)
		);

		Widget updateUserForm = SingleChildScrollView(
			child: Form(
				key: _formKey,
				child: Column(
					children: [
						txtName,
						txtEmail,
						txtPhone,
						txtWebsite,
						btnUpdate
					]
				)
			)
		);

		Widget updateUserView = FutureBuilder(
			future: _futureUser,
			builder: (BuildContext context, response) {
				if (response.connectionState == ConnectionState.done && response.hasData == true) {
					Timer(Duration(milliseconds: 500), () {
                        showSnackBar(context, 'User information updated successfully.');
                        Navigator.pop(context);
                    });
				}

				return updateUserForm;
			}
		);

		return Scaffold(
			appBar: AppBar(title: Text('Update User')),
			body: Container(
				padding: EdgeInsets.all(22.0),
				child: updateUserView
			)
		);
	}
}
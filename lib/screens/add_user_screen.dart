import 'dart:async';
import 'package:flutter/material.dart';

import '/utils/api.dart';
import '/utils/functions.dart';
import '/models/user.dart';

class AddUserScreen extends StatefulWidget {
	@override
	AddUserScreenState createState() => AddUserScreenState();
}

class AddUserScreenState extends State<AddUserScreen> {
	Future<User>? _futureUser;

	final _formKey = GlobalKey<FormState>();
	final _tffNameController = TextEditingController();
	final _tffEmailController = TextEditingController();
	final _tffPhoneController = TextEditingController();
	final _tffWebsiteController = TextEditingController();

	void _addUser(BuildContext context) {
		User user = User(
			name: _tffNameController.text,
			email: _tffEmailController.text,
			phone: _tffPhoneController.text,
			website: _tffWebsiteController.text
		);

		setState(() {
			_futureUser = API().addUser(user).catchError((e) {
				showSnackBar(context, e.message);
				clearFields(_tffNameController, _tffEmailController, _tffPhoneController, _tffWebsiteController);
			});
		});
	}

	@override
	Widget build(BuildContext context) {
		Widget txtName = TextFormField(
			decoration: InputDecoration(labelText: 'Name'),
			controller: _tffNameController,
			validator: (value) {
				return value != null && value.isNotEmpty ? null : 'Name is required.';
			}
		);

		Widget txtEmail = TextFormField(
			decoration: InputDecoration(labelText: 'Email'),
			controller: _tffEmailController,
			validator: (value) {
				return value != null && value.isNotEmpty && value.contains('@') ? null : 'Email is invalid.';
			}
		);

		Widget txtPhone = TextFormField(
			decoration: InputDecoration(labelText: 'Phone'),
			controller: _tffPhoneController,
			validator: (value) {
				return value != null && value.isNotEmpty ? null : 'Phone is required.';
			}
		);

		Widget txtWebsite = TextFormField(
			decoration: InputDecoration(labelText: 'Website'),
			controller: _tffWebsiteController,
			validator: (value) {
				return value != null && value.isNotEmpty ? null : 'Website is required.';
			}
		);

		Widget btnAdd = Container(
			width: double.infinity,
			margin: EdgeInsets.only(top: 10.0),
			child: ElevatedButton(
				child: Text('Add'),
				onPressed: () {
					if (_formKey.currentState!.validate()) {
						_addUser(context);
					} else {
						showSnackBar(context, 'Invalid inputs.');
					}
				}
			)
		);

		Widget addUserForm = SingleChildScrollView(
			child: Form(
				key: _formKey,
				child: Column(
					children: [
						txtName,
						txtEmail,
						txtPhone,
						txtWebsite,
						btnAdd
					]
				)
			)
		);

		Widget addUserView = FutureBuilder(
			future: _futureUser,
			builder: (BuildContext context, response) {
				if (response.connectionState == ConnectionState.done && response.hasData == true) {
					Timer(Duration(milliseconds: 500), () {
                        showSnackBar(context, 'User added successfully.');
                        clearFields(_tffNameController, _tffEmailController, _tffPhoneController, _tffWebsiteController);
                        Navigator.pushReplacementNamed(context, '/');
                    });
				}

				return addUserForm;
			}
		);

		return Scaffold(
			appBar: AppBar(title: Text('Add User')),
			body: Container(
				padding: EdgeInsets.all(22.0),
				child: addUserView 
			)
		);
	}
}
import 'package:flutter/material.dart';

import '/utils/api.dart';
import '/models/user.dart';
import '/screens/edit_user_screen.dart';
import '/widgets/app_drawer.dart';

class UserListScreen extends StatefulWidget {
	@override
	UserListScreenState createState() => UserListScreenState();
}

class UserListScreenState extends State<UserListScreen> {
	Future? futureUsers; 
	final refreshIndicatorKey = GlobalKey<RefreshIndicatorState>(); 

	Widget userCard(User user) {
		return Card(
			shape: RoundedRectangleBorder(
				borderRadius: BorderRadius.all(Radius.circular(10.0))
			),
			child: ExpansionTile(
				backgroundColor: Colors.white70,
				title: Text(user.name),
				trailing: Text(''),
				children: [
					Padding(
						padding: EdgeInsets.all(8.0),
						child: Column(
							children: [	
								ListTile(
									title: Text(user.name),
									subtitle: Text('Name'),
									trailing: Icon(Icons.edit, color: Colors.blue),
									onTap: () {
										Navigator.push(context, MaterialPageRoute(builder: (context) => EditUserScreen(user)));
									}
								),										
								ListTile(
									title: Text(user.email),
									subtitle: Text('Email')
								),										
								ListTile(
									title: Text(user.phone),
									subtitle: Text('Phone')
								),										
								ListTile(
									title: Text(user.website),
									subtitle: Text('Website')
								),	
							]
						)
					)
				],
			)
		);
	}

	List<Widget> generateListTiles(List<User> users) {
		List<Widget> listTiles = [];

		for (User user in users) {
			listTiles.add(userCard(user));
		}

		return listTiles;
	}

	@override
	void initState() {
		super.initState();

		WidgetsBinding.instance!.addPostFrameCallback((timestamp) {
			setState(() {
				futureUsers = API().getUsers();
			});
		});
	}

	@override
	Widget build(BuildContext context) {
		Widget fbUserList = FutureBuilder(
			future: futureUsers,
			builder: (BuildContext context, snapshot) {
				if (snapshot.connectionState == ConnectionState.done) {
					if (snapshot.hasError) {
						return Center(
							child: Text('Could not load the user list, restart the app.')
						);
					}

					return RefreshIndicator(
						key: refreshIndicatorKey,
						onRefresh: () async {
							setState(() {
								futureUsers = API().getUsers();
							});
						},
						child: ListView(
							padding: EdgeInsets.all(8.0),
							children: generateListTiles(snapshot.data as List<User>)
						)
					);
				}

				return Center(
					child: CircularProgressIndicator()
				);
			}
		);

		Widget fbaAddUser = FloatingActionButton(
			child: Icon(Icons.add),
			onPressed: () {
				Navigator.pushNamed(context, '/add-user');
			}
		);

		return Scaffold(
			appBar: AppBar(title: Text('User List')),
			endDrawer: AppDrawer(),
			body: Container(
				child: fbUserList
			),
			floatingActionButton: fbaAddUser
		);
	}
}
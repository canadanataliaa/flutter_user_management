import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart';
part 'user.g.dart';

@freezed
class User with _$User {
	const factory User({
		int? id,
		required String name,
		required String email,
		Map<String, dynamic>? address,
		required String phone,
		required String website,
		Map<String, String>? company
	}) = _User;

	factory User.fromJson(Map<String, dynamic> data) => _$UserFromJson(data);
}
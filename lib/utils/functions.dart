import 'package:flutter/material.dart';

void showSnackBar(BuildContext context, String message) {
    SnackBar snackBar = new SnackBar(
        content: Text(message), 
        duration: Duration(milliseconds: 3500)
    );
    
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

void clearFields(n, e, p, c) {
    n.text = '';
    e.text = '';
    p.text = '';
    c.text = '';
}
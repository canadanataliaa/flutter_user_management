import 'dart:async';

import 'package:dio/dio.dart';

import '/models/user.dart';

class API {
	final String url = 'https://jsonplaceholder.typicode.com/users';

	Future getUsers() async {
		try {
			var response = await Dio().get(url);
			List<User> users = [];

			// print(response.data);
			// response.data.map((user) => users.add(User.fromJson(user)));

			for (var user in response.data) {
				// The 'user' here is of Map<String, dynamic> type.
				// The 'user' map is converted into a User object.
				// After conversion, the user object is added to the list.
				users.add(User.fromJson(user));
			}

			return users;
		} catch (exception) {
			throw exception;
		}
	}

	Future<User> addUser (User user) async {
		User? addedUser;

		try {
			var response = await Dio().post(
				url,
				options: Options(
					headers: { 'Content-Type': 'application/json; charset=UTF-8' }
				),
				data: user.toJson()
			);

			addedUser = User.fromJson(response.data);

			if (response.statusCode == 201) {
                print(addedUser);
				return addedUser;
			} else {
				throw Exception('Failed to add user.');
			}
		} catch(exception) {
			throw exception;
		}
	}


	Future<User> updateUser (User user, int? id) async {
		User? updatedUser;

		try {
			var response = await Dio().put(
				url + '/$id',
				data: user.toJson()
			);

			updatedUser = User.fromJson(response.data);

			if (response.statusCode == 200) {
                print(updatedUser);
				return updatedUser;
			} else {
				throw Exception('Failed to update user information.');
			}
		} catch(exception) {
			throw exception;
		}
	}
}
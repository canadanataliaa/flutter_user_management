import 'package:flutter/material.dart';

class AppDrawer extends StatelessWidget {
	@override
	Widget build(BuildContext context) {
		return Drawer(
			child: ListView(
				padding: EdgeInsets.zero,
				children: [
					DrawerHeader(
						decoration: BoxDecoration(color: Colors.blue),
						child: Center(child: Text('User Management')),
					),
					ListTile(
						leading: Icon(Icons.people, color: Colors.black),
						title: Text('Users', style: TextStyle(color: Colors.black)),
						onTap: () => Navigator.pushNamed(context, '/')
					),												
					ListTile(
						leading: Icon(Icons.person_add_alt_rounded, color: Colors.black),
						title: Text('Add user', style: TextStyle(color: Colors.black)),
						onTap: () => Navigator.pushNamed(context, '/add-user')
					)
				]
			)
		);
	}
}


